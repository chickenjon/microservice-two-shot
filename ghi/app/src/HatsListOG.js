import React, {useEffect} from "react";

function GenerateColumn(props) {
  return (
    <div className="col">
      {props.column.map(item => {
        return (
          <div key={item.id} className="card mb-3 shadow">
            <img src={item.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{item.style_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {item.fabric}, {item.color}
              </h6>
            </div>
            <div className="card-footer">
              <span>{item.location.closet_name} - {item.location.section_number}/{item.location.shelf_number}</span>
              <span style={{float:'right'}}>
                <button className="btn btn-primary" onClick={() => props.this.edithat(item)}>Edit</button>
                <span> </span>
                <button className="btn btn-danger" onClick={() => props.this.deleteHat(item)}>Delete</button>
              </span>
            </div>
          </div>
        );
      })}
    </div>
  );
}

class HatsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hats: [],
      columns: [[], [], []],
    }

    this.deleteHat = this.deleteHat.bind(this);
  }

  sortColumns(items) {
    const columns = [[], [], []];

      let i = 0;
      for (const item of items) {
        columns[i].push(item);
        i = i + 1;
        if (i > 2) {
          i = 0;
        }
      }

      this.setState({columns: columns});
  }

  async componentDidMount() {
    const response = await fetch('http://localhost:8090/api/hats/')
    if (response.ok) {
      const data = await response.json()
      this.setState({hats: data.hats})

      this.sortColumns(this.state.hats)
    }
  }

  async deleteHat(item) {
    console.log(item)
    const deleteUrl = `http://localhost:8090/api/hats/${item.id}`
    const fetchConfig = {
      method: "delete"
    }
    await fetch(deleteUrl, fetchConfig)

    const idx = this.state.hats.indexOf(item)
    const updated_hats = [...this.state.hats]
    updated_hats.splice(idx, 1)
    this.setState({ hats: updated_hats })
    this.sortColumns(this.state.hats)
  }

  render() {
    return (
      <div className="row mt-5">
        {this.state.columns.map((column, index) => {
          return (
            <GenerateColumn key={index} column={column} this={this} />
          );
        })}
      </div>
      // <table className="table table-striped">
      //   <thead>
      //     <tr>
      //       <th>Image</th>
      //       <th>Style Name</th>
      //       <th>Fabric</th>
      //       <th>Color</th>
      //       <th>Location</th>
      //       <th> </th>
      //       <th> </th>
      //     </tr>
      //   </thead>
      //   <tbody>
      //     {this.state.hats.map(hat => {
      //       return (
      //         <tr key={hat.id}>
      //           <td><img src={hat.picture_url} width="300" /></td>
      //           <td>{hat.style_name}</td>
      //           <td>{hat.fabric}</td>
      //           <td>{hat.color}</td>
      //           <td>{hat.location.closet_name} - section {hat.location.section_number} / shelf {hat.location.shelf_number}</td>
      //           <td><button className="btn btn-primary" onClick={() => this.edithat(hat)}>Edit</button></td>
      //           <td><button className="btn btn-danger" onClick={() => this.deleteHat(hat)}>Delete</button></td>
      //         </tr>
      //       );
      //     })}
      //   </tbody>
      // </table>
    )
  }
}

export default HatsList